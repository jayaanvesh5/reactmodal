import { useState } from 'react';
import './App.scss';
import Button from './components/button/Button';
import Modal from './components/modal/CustomModal';

function App() {
  const [showModal, setShowModal] = useState(false);

  return (
    <div>
      <Button onClick={() => setShowModal(true)}>Open modal</Button>
      <Modal
        show={showModal}
        setShow={setShowModal}
        // closeButton={true}
        position='bottom'
        contentStyle={{ borderTopLeftRadius: 20 }}
      >
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt maxime dolorem
          asperiores laboriosam ad delectus ea. Tempora tempore repellendus laudantium fugiat saepe
          mollitia eius illo possimus laborum consequuntur, tenetur neque.
        </p>
      </Modal>
    </div>
  );
}

export default App;
