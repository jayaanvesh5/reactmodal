import { useEffect, useRef } from 'react';
import './modal.css';

const CustomModal = (props) => {
  const modalRef = useRef();

  useEffect(() => {
    const clickOutsideContent = (e) => {
      if (e.target === modalRef.current) {
        props.setShow(false);
      }
    };
    window.addEventListener('click', clickOutsideContent);
    return () => {
      window.removeEventListener('click', clickOutsideContent);
    };
  }, [props]);

  const positionHandler = () => {
    let key;
    switch (props.position) {
      case 'top':
        key = 'customModalTop';
        break;
      case 'bottom':
        key = 'customModalBottom';
        break;
      case 'center':
        key = 'customModalCenter';
        break;
      default:
        key = 'customModalCenter';
        break;
    }
    return key;
  };

  return (
    <div ref={modalRef} className={`customModal ${props.show ? 'customModalActive' : ''}`}>
      <div className={`customModalPosition ${positionHandler()}`}>
        <div className='customModalContainer'>
          <div className='customModalContent' style={props.contentStyle}>
            {props.closeButton && (
              <span
                onClick={() => props.setShow(false)}
                className='customModalClose'
                style={props.closeBtnStyle}
              >
                &times;
              </span>
            )}
            {props.children}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomModal;

CustomModal.defaultProps = {
  contentStyle: {},
  closeButton: false,
  closeBtnStyle: {},
  position: 'center',
};
